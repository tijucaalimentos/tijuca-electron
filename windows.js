var electronInstaller = require('electron-winstaller');
const path = require('path')

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: './release-builds/tijuca-mfe-win32-x64',
    outputDirectory: './release-builds/',
    authors: 'Tijuca Alimentos LTDA',
    noMsi: true,
    exe: 'tijuca-mfe.exe',
    setupExe: 'TijucaMFE.exe',
  });
 
resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));